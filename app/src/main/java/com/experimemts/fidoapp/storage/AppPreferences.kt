package com.experimemts.fidoapp.storage

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class AppPreferences (appContext: Context) {

    private val USER_CREDENTIALS = "USER_CREDENTIALS"

    lateinit var preferences : SharedPreferences

    init {
        preferences = appContext.getSharedPreferences("prefs", MODE_PRIVATE )
    }

    fun setCredentials( value : String? ) = setValue(USER_CREDENTIALS, if (value == null) "" else value )
    fun getCredentials() : String = getValue(USER_CREDENTIALS)

    private fun setValue(key:String, value:String) {
        with (preferences.edit()) {
            putString(key, value)
            commit()
        }
    }

    private fun getValue(key:String) : String = preferences.getString(key, "")!!
}