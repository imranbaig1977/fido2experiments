package com.experimemts.fidoapp

import android.app.Application
import com.experimemts.fidoapp.storage.AppPreferences

class FidoApp : Application() {

    lateinit var preferences: AppPreferences

    override fun onCreate() {
        super.onCreate()
        preferences = AppPreferences(this)

    }
}