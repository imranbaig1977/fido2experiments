package com.experimemts.fidoapp.utils

import android.net.Uri
import java.lang.Exception

class UserCredentials (var user:String?, var credentialId : String?) {
    companion object {
        fun parse(data : String) : UserCredentials {
                val uri =
                    Uri.parse(data)
                var user = uri.path
                var credentialId = uri.getQueryParameter("credentialId")
                return UserCredentials(user, credentialId)

        }

        fun isRegisterSuccess( data :String?) : Boolean{

            val uri =
                Uri.parse(data)
            var credentialId = uri.getQueryParameter("credentialId")

            return !credentialId.isNullOrBlank()
        }
    }
}