package com.experimemts.fidoapp.ui

import android.net.Uri
import android.os.Bundle
import com.experimemts.fidoapp.R
import com.experimemts.fidoapp.ui.base.BaseActivity
import androidx.browser.customtabs.*
import com.experimemts.fidoapp.BuildConfig
import com.experimemts.fidoapp.utils.UserCredentials
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URLEncoder

class MainActivity : BaseActivity()  {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonRegister.setOnClickListener {
            var customTabsIntent = CustomTabsIntent.Builder().build()
            customTabsIntent.launchUrl(this, Uri.parse(BuildConfig.SERVER_URL_REGISTER))
        }

        buttonLogin.setOnClickListener {

            var user = UserCredentials.parse(getApp().preferences.getCredentials())


            var credentials = URLEncoder.encode(user.credentialId, "UTF-8");

            credentials = credentials.replace( "+",  "%2B")

            val url = "${BuildConfig.SERVER_URL_LOGIN}${credentials}"

            var customTabsIntent = CustomTabsIntent.Builder().build()
            customTabsIntent.launchUrl(this, Uri.parse(url))
        }

        imageViewDelete.setOnClickListener {
            getApp().preferences.setCredentials("")
            refresh()
        }
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    fun refresh() {

        var credentialsData = getApp().preferences.getCredentials()
        buttonLogin.isEnabled = !credentialsData.isNullOrBlank()
        imageViewDelete.isEnabled = !credentialsData.isNullOrBlank()
        textViewUserCredentials.text = credentialsData

    }
}