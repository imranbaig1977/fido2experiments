package com.experimemts.fidoapp.ui.browsertab

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.experimemts.fidoapp.R
import com.experimemts.fidoapp.ui.base.BaseActivity
import com.experimemts.fidoapp.utils.UserCredentials
import kotlinx.android.synthetic.main.activity_browser_tab.*

class BrowserTabActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_browser_tab)

        if (savedInstanceState == null){

            Log.d("BrowserTabActivity", "" + intent?.dataString)

            if(UserCredentials.isRegisterSuccess( intent?.dataString )) {
                getApp().preferences.setCredentials( intent?.dataString )
                textView.text =  "Successfully registered\n${intent?.dataString}"
            }
            else {
                textView.text =  "Successfully Signed in\n${intent?.dataString}"
            }
        }

        button.setOnClickListener {
            finish()
        }
    }

}