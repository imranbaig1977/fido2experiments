package com.experimemts.fidoapp.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.experimemts.fidoapp.FidoApp

open class BaseActivity : AppCompatActivity() {
    fun getApp() : FidoApp = application as FidoApp
}